//
//  HomeViewController.swift
//  Test for Albo
//
//  Created by Jaime Zayas on 2/17/20.
//  Copyright © 2020 Jaime Zayas. All rights reserved.
//

import UIKit
import CoreLocation

class HomeViewController: UIViewController {

    private var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLocationManager()
    }

    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(true)
        getCurrentLocationStatus()
    }
    
    private func initLocationManager(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    
    private func getCurrentLocationStatus(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    notifyAccessRestricted()
                default:
                break
            }
            } else {
                print("Location services are not enabled")
        }
    }
    
    private func redirectToDeviceSettings(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }

    }
    
    /*
     override func viewDidAppear(_ animated: Bool) {
         let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .alert)

         let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

         }
         alertController.addAction(settingsAction)
         let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
         alertController.addAction(cancelAction)

         present(alertController, animated: true, completion: nil)
     }
     */
    
    private func notifyAccessRestricted(){
        let userAlert = UIAlertController(title: "Problemas de acceso de Localización", message: "Para poder usar este aplicativo, considere otorgar permisos de localización", preferredStyle: UIAlertController.Style.alert)
        userAlert.addAction(UIAlertAction(title: "Ir a Ajustes", style: UIAlertAction.Style.default) { action in
            self.redirectToDeviceSettings()
        })
        userAlert.addAction(UIAlertAction(title: "Ahora no", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(userAlert, animated: true, completion: nil)
    }
    
}
extension HomeViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways{
            print("status authorized")
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self){
                print("is monitoring")
                if CLLocationManager.isRangingAvailable() {
                    print("scanning")
                    startScanning()

                }
            }
        }
    }
    
    
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .authorizedAlways {
//            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
//                if CLLocationManager.isRangingAvailable() {
//                    startScanning()
//                }
//            }
//        }
//    }
    
    func startScanning() {
//Personal: 17e91476b98a8a8b90e1d5323e064f960d294551--- AD899ABE-5892-432A-BC44-ECC16A9A60EE
//iphone 7: e79604268684a69d5a4db1e36ce35d5f44edfc9a
//Mac : 06660CD3-CFCD-5E41-A77C-49CBEDE98C17

        let uuid = UUID(uuidString: "06660CD3-CFCD-5E41-A77C-49CBEDE98C17")!
        //let uuid = UUID(uuidString: "e79604268684a69d5a4db1e36ce35d5f44edfc9a")!
        //let beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: 123, minor: 456, identifier: "MyBeacon")
        
        let beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: 123, minor: 456, identifier: "MyBeacon")

        
        let constraint: CLBeaconIdentityConstraint = CLBeaconIdentityConstraint(uuid: uuid)
        
        locationManager.startMonitoring(for: <#T##CLRegion#>))
//        locationManager.startMonitoring(for: beaconRegion)
        
        locationManager.startRangingBeacons(satisfying: constraint)
        
        //locationManager.startRangingBeacons(in: beaconRegion)
    }
  
    
    

    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        if beacons.count > 0 {
            updateDistance(beacons[0].proximity)
        } else {
            updateDistance(.unknown)
        }
    }

    func updateDistance(_ distance: CLProximity) {
        UIView.animate(withDuration: 0.8) {
            switch distance {
            case .unknown:
                self.view.backgroundColor = UIColor.gray

            case .far:
                self.view.backgroundColor = UIColor.blue

            case .near:
                self.view.backgroundColor = UIColor.orange

            case .immediate:
                self.view.backgroundColor = UIColor.red
            @unknown default:
                print("Not working")
            }
        }
    }
    
}
